﻿using BookLibrary.Models;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary
{
    public class BookAuthor
    {
        public int AuthorId { get; set; }
        public Author Author { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }
    }
    
    public class BookGenre
    {
        public int GenreId { get; set; }
        public Genre Genre { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }
    }

    public class BookPublisher
    {
        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }
    }

    public class BookReview
    {
        public int ReviewId { get; set; }
        public Review Review { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }
    }

    public class ApplicationContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Publisher> Publishers { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
            //Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var adminRole = new Role { Id = 1, Name = "admin" };
            var librarianRole = new Role { Id = 2, Name = "librarian" };
            var userRole = new Role { Id = 3, Name = "user" };

            User admin = new User {
                Id = 1,
                Name = "admin",
                Email = "thejustwatch@gmail.com",
                Description = "Администратор",
                Password = "12345admin",
                RoleId = adminRole.Id
            };

            User librarian = new User
            {
                Id = 2,
                Name = "librarian",
                Email = "librarian@gmail.com",
                Description = "Библиотекарь",
                Password = "12345lib",
                RoleId = librarianRole.Id
            };

            User user = new User
            {
                Id = 3,
                Name = "user",
                Email = "user@gmail.com",
                Description = "Пользователь",
                Password = "12345user",
                RoleId = userRole.Id
            };

            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<Role>()
                .HasData(new Role[] { adminRole, librarianRole, userRole });

            modelBuilder.Entity<User>()
                .HasData(new User[] { admin, librarian, user });



            modelBuilder.Entity<BookAuthor>()
                .HasKey(t => new { t.BookId, t.AuthorId, });

            modelBuilder.Entity<BookGenre>()
                .HasKey(t => new { t.BookId, t.GenreId, });

            modelBuilder.Entity<BookPublisher>()
                .HasKey(t => new { t.BookId, t.PublisherId });

            modelBuilder.Entity<BookReview>()
                .HasKey(t => new { t.BookId, t.ReviewId });



            modelBuilder.Entity<BookAuthor>()
                .HasOne(sc => sc.Book)
                .WithMany(s => s.BookAuthors)
                .HasForeignKey(sc => sc.BookId);

            modelBuilder.Entity<BookGenre>()
                .HasOne(sc => sc.Book)
                .WithMany(s => s.BookGenres)
                .HasForeignKey(sc => sc.BookId);

            modelBuilder.Entity<BookPublisher>()
                .HasOne(sc => sc.Book)
                .WithMany(s => s.BookPublishers)
                .HasForeignKey(sc => sc.BookId);

            modelBuilder.Entity<BookReview>()
                .HasOne(sc => sc.Book)
                .WithMany(s => s.BookReviews)
                .HasForeignKey(sc => sc.BookId);



            modelBuilder.Entity<BookAuthor>()
                .HasOne(sc => sc.Author)
                .WithMany(s => s.BookAuthors)
                .HasForeignKey(sc => sc.AuthorId);

            modelBuilder.Entity<BookGenre>()
                .HasOne(sc => sc.Genre)
                .WithMany(s => s.BookGenres)
                .HasForeignKey(sc => sc.GenreId);

            modelBuilder.Entity<BookPublisher>()
                .HasOne(sc => sc.Publisher)
                .WithMany(s => s.BookPublishers)
                .HasForeignKey(sc => sc.PublisherId);

            modelBuilder.Entity<BookReview>()
                .HasOne(sc => sc.Review)
                .WithMany(s => s.BookReviews)
                .HasForeignKey(sc => sc.ReviewId);
        }
    }
}
