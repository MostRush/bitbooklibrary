﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using BookLibrary.Models;
using BookLibrary.Models.View;
using BookLibrary.Models.View.Authentication;
using HtmlAgilityPack;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;

namespace BookLibrary.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationContext db;
        private IWebHostEnvironment hostingEnvironment;

        public AccountController(ApplicationContext context, IWebHostEnvironment environment)
        {
            hostingEnvironment = environment;
            db = context;
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("index", "home");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await db.Users
                    .Include(u => u.Role)
                    .FirstOrDefaultAsync(u => (u.Email == model.Login ||
                    u.Name == model.Login) && u.Password == model.Password);

                if (user != null)
                {
                    await Authenticate(user);

                    var referer = HttpContext.Request.Headers["Referer"].ToString();

                    return Redirect(referer);
                }

                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, bool fromControlPanel)
        {
            var users = db.Users.Select(x => x).ToList();

            if (ModelState.IsValid)
            {
                var user = await db.Users
                    .FirstOrDefaultAsync(u => u.Email == model.Email || u.Name == model.Name);

                if (user is null)
                {
                    user = new User
                    {
                        Name = model.Name,
                        Email = model.Email,
                        Password = model.Password,
                    };

                    Role userRole = await db.Roles.FirstOrDefaultAsync(r => r.Name == "user");

                    if (userRole != null)
                        user.Role = userRole;

                    await db.Users.AddAsync(user);

                    await db.SaveChangesAsync();

                    if (fromControlPanel)
                    {
                        var referer = HttpContext.Request.Headers["Referer"].ToString();

                        return Redirect(referer);
                    }
                    else
                    {
                        await Authenticate(user);
                        return RedirectToAction("index", "home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Пользователь с такой почтой или именем уже существует!");
                }
            }

            return View(model);
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Name),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name)
            };

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            var referer = HttpContext.Request.Headers["Referer"].ToString();

            return Redirect(referer);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> GetFeedback(ReviewBookViewModel model, int reviewBookId)
        {
            if (ModelState.IsValid)
            {
                var bookHaveUserReview = await db.Reviews.AnyAsync(
                    r => r.User.Name == User.Identity.Name && r.BookId == reviewBookId);

                if (!bookHaveUserReview)
                {
                    var review = new Review()
                    {
                        Rate = model.RateValue ?? 0,
                        Comment = model.Comment,
                        PublicationDate = DateTime.Now,
                        User = await db.Users.FirstOrDefaultAsync(x => x.Name == User.Identity.Name),
                        Book = await db.Books.FirstOrDefaultAsync(x => x.Id == reviewBookId)
                    };

                    await db.Reviews.AddAsync(review);
                    await db.SaveChangesAsync();

                    await CalcAvrgRate(reviewBookId);
                }

                return RedirectToAction("bookpage", "library", new { bookId = reviewBookId });
            }

            return RedirectToAction("bookpage", "library", new { bookId = reviewBookId });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> RemoveFeedback(int? id, int? bookId)
        {
            var review = await db.Reviews.Include(x => x.User).FirstOrDefaultAsync(r => r.Id == id);

            if (User.Claims.FirstOrDefault(
                x => x.Value == "admin" || x.Value == "librarian") != null)
            {
                db.Reviews.Remove(review);
            }
            else
            {
                if (review.User.Name == User.Identity.Name)
                {
                    db.Reviews.Remove(review);
                }
            }

            await db.SaveChangesAsync();
            await CalcAvrgRate(review.BookId);

            var referer = HttpContext.Request.Headers["Referer"].ToString();
            return Redirect(referer);
        }

        [HttpGet]
        public async Task<IActionResult> AccountPage(string user)
        {
            var u = await db.Users
                .Include(x => x.Role)
                .FirstOrDefaultAsync(u => u.Name == user);

            if (u != null)
            {
                return View(u);
            }

            return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> UserReviews(string user, int itemsQuantity = 8,
            int reviewsQuantity = 4, ReviewSortState sortOrder = ReviewSortState.DateDesc)
        {
            var u = await db.Users
                .FirstOrDefaultAsync(u => u.Name == user);

            if (u != null)
            {
                var reviews = db.Reviews
                        .Where(x => x.UserId == u.Id)
                        .Include(x => x.Book)
                        .Select(x => x)
                        .OrderByDescending(x => x.PublicationDate);

                reviews = sortOrder switch
                {
                    ReviewSortState.DateAsc => reviews.OrderBy(r => r.PublicationDate),
                    ReviewSortState.DateDesc => reviews.OrderByDescending(r => r.PublicationDate),
                    ReviewSortState.RatingAsc => reviews.OrderBy(r => r.Rate),
                    ReviewSortState.RatingDesc => reviews.OrderByDescending(r => r.Rate),
                    _ => reviews.OrderByDescending(r => r.PublicationDate)
                };

                var model = new UserRviewsViewModel
                {
                    User = u,
                    Reviews = await reviews.ToListAsync(),
                    SortViewModel = new ReviewSortViewModel(sortOrder),
                    ItemsQuantity = itemsQuantity
                };

                ViewBag.ReviewsQuantity = reviewsQuantity;
                return View(model);
            }

            return NotFound();
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Settings(string user)
        {
            if (user != User.Identity.Name)
                return NotFound();

            var usr = await db.Users
                .Include(x => x.Role)
                .FirstOrDefaultAsync(u => u.Name == user);

            if (usr != null)
            {
                var userModel = new UserSettingsViewModel
                {
                    Name = usr.Name,
                    Email = usr.Email,
                    Description = usr.Description,
                    User = usr
                };

                return View(userModel);
            }

            return NotFound();
        }

        [HttpPost]
        [Authorize]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> ChangeAvatar(UserSettingsViewModel model, string username)
        {
            if (username != User.Identity.Name)
                return NotFound();

            /* Перевод изображения в массив байтов */

            var files = HttpContext.Request.Form.Files;
            byte[] bookImage = null;

            foreach (var image in files)
            {
                if (image != null && image.Length > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await image.OpenReadStream().CopyToAsync(memoryStream);
                        bookImage = memoryStream.ToArray();
                    }
                    break;
                }
            }

            var user = await db.Users.FirstOrDefaultAsync(x => x.Name == username);

            if (user != null)
            {
                user.Avatar = bookImage;

                db.Users.Update(user);
                await db.SaveChangesAsync();
            }

            var referer = HttpContext.Request.Headers["Referer"].ToString();
            return Redirect(referer);
        }

        [HttpPost]
        [Authorize]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> ChangeName(UserSettingsViewModel model, string username)
        {
            if (username != User.Identity.Name)
                return NotFound();

            var user = await db.Users.Include(x => x.Role).FirstOrDefaultAsync(x => x.Name == username);
            var existingName = await db.Users.AnyAsync(u => u.Name == model.Name);

            if (user != null)
            {
                if (user.Name != model.Name && !existingName)
                    user.Name = model.Name;
                else
                {
                    ModelState.AddModelError("NameErrors", "Пользователь с таким именем уже существует!");
                }

                db.Users.Update(user);
                await db.SaveChangesAsync();
            }

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            await Authenticate(user);

            return RedirectToAction("settings", "account", new { user = user.Name });
        }

        [HttpPost]
        [Authorize]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> ChangeDescription(UserSettingsViewModel model, string username)
        {
            if (username != User.Identity.Name)
                return NotFound();

            var user = await db.Users.Include(x => x.Role).FirstOrDefaultAsync(x => x.Name == username);

            if (user != null)
            {
                user.Description = model.Description;
                db.Users.Update(user);
                await db.SaveChangesAsync();
            }

            return RedirectToAction("settings", "account", new { user = user.Name });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SubmitEmail(UserSettingsViewModel model, string username)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                return RedirectToAction("PassEmailSent", "account", "Ошибка");
            }
            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("BookLibrary", "thejustwatch@gmail.com"));
            message.To.Add(new MailboxAddress(model?.Name ?? "Unknown", model.Email));
            message.Subject = "BookLibrary. Восстановление пароля.";

            var builder = new BodyBuilder();
            var html = await System.IO.File.ReadAllTextAsync(hostingEnvironment.WebRootPath + @"\html\email.html");

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.Load(hostingEnvironment.WebRootPath + @"\html\email.html");

            foreach (HtmlNode link in htmlDocument.DocumentNode.SelectNodes("//a[@id='email']"))
            {
                link.InnerHtml = model.Email;
            }

            foreach (HtmlNode link in htmlDocument.DocumentNode.SelectNodes("//form[@id='action']"))
            {
                link.SetAttributeValue("action", "https://vk.com");
            }

            foreach (HtmlNode link in htmlDocument.DocumentNode.SelectNodes("//img[@id='logo']"))
            {
                link.SetAttributeValue("src", @$"{hostingEnvironment.WebRootPath}\img\logo.png");
            }

            

            builder.HtmlBody = htmlDocument.DocumentNode.OuterHtml;

            message.Body = builder.ToMessageBody();
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 465, true);
                await client.AuthenticateAsync("booklibrary256@gmail.com", "123booklib");
                await client.SendAsync(message);

                await client.DisconnectAsync(true);
            }

            return RedirectToAction("PassEmailSent", "account", model.Email);
        }

        public IActionResult PassEmailSent(string email)
        {
            var model = new RestoreViewModel
            {
                Email = email
            };

            return View(model);
        }

        public async Task<IActionResult> ChangePassword(UserSettingsViewModel model, int userId)
        {
            var user = await db.Users.FirstOrDefaultAsync(x => x.Id == userId);

            if (user != null)
            {
                if (user.Password == model.OldPassword)
                {
                    user.Password = model.NewPassword;
                    db.Users.Update(user);
                    await db.SaveChangesAsync();

                    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                } 
                else
                {
                    ModelState.AddModelError("", "Старый пароль неверен!");
                }
            }

            return RedirectToAction("settings", "account", new { user = user.Name });
        }

        public async Task<IActionResult> RestorePass(RestoreViewModel model)
        {
            var user = await db.Users.FirstOrDefaultAsync(x => x.Email == model.Email);

            if (user == null)
            {
                return RedirectToAction("PassEmailSent", "account", model.Email);
            }

            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("BookLibrary", "thejustwatch@gmail.com"));
            message.To.Add(new MailboxAddress(model.Name, model.Email));
            message.Subject = "BookLibrary. Восстановление пароля.";

            var builder = new BodyBuilder();
            var html = await System.IO.File.ReadAllTextAsync(@"C:\Users\MostRush\source\repos\bitbooklibrary\BookLibrary\BookLibrary\wwwroot\html\pass.html");

            // Set the html version of the message text
            builder.HtmlBody = string.Format(html);

            // Now we just need to set the message body and we're done
            message.Body = builder.ToMessageBody();
            /*
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 465, true);
                await client.AuthenticateAsync("booklibrary256@gmail.com", "123booklib");
                await client.SendAsync(message);

                await client.DisconnectAsync(true);
            }
            */
            return RedirectToAction("PassEmailSent", "account", model.Email);
        }

        private async Task CalcAvrgRate(int bookId)
        {
            var book = await db.Books
                .Include(b => b.Reviews)
                .ThenInclude(sc => sc.User)
                .FirstOrDefaultAsync(x => x.Id == bookId);

            double rateSumm = 0;
            var count = book.Reviews.Count;

            foreach (var r in book.Reviews)
                rateSumm += r.Rate;

            book.Rating = count > 0 ? rateSumm / count : 0;

            await db.SaveChangesAsync();
        }
    }
}
