﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BookLibrary.Models;
using BookLibrary.Models.View;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Controllers
{
    [Authorize(Roles = "admin, librarian")]
    public class AdminController : Controller
    {
        private ApplicationContext db;
        private IWebHostEnvironment webHostEnvironment;

        public AdminController(ApplicationContext context, IWebHostEnvironment environment)
        {
            db = context;
            webHostEnvironment = environment;

            if (db.Authors.Count() == 0)
            {
                Author[] authors =
                {
                    new Author { FirstName = "Илон", LastName = "Маск", Biography = "Гений" },
                    new Author { FirstName = "Данил", LastName = "Щукин", Biography = "Психолог" },
                    new Author { FirstName = "Аркадий", LastName = "Стругацкий", Biography = "Фантаст" },
                    new Author { FirstName = "Борис", LastName = "Стругацкий", Biography = "Фантаст" },
                    new Author { FirstName = "Кэл", LastName = "Ньюпорт", Biography = "Программист" },
                    new Author { FirstName = "Гордон", LastName = "Рамзи", Biography = "Кулинар" },
                };

                db.Authors.AddRange(authors);
                db.SaveChanges();
            }

            if (db.Genres.Count() == 0)
            {
                Genre[] genres =
                {
                    new Genre { Name = "Биографии и мемуары", Description = "" },
                    new Genre { Name = "Менеджемент", Description = "" },
                    new Genre { Name = "Кулинария", Description = "" },
                    new Genre { Name = "Психология", Description = "" },
                    new Genre { Name = "Программирование", Description = "" },
                    new Genre { Name = "Бизнес", Description = "" },
                };

                db.Genres.AddRange(genres);
                db.SaveChanges();
            }

            if (db.Publishers.Count() == 0)
            {
                Publisher[] publishers =
                {
                    new Publisher { Name = "Эксмо – АСТ", Description = "" },
                    new Publisher { Name = "Азбука – Аттикус", Description = "" },
                    new Publisher { Name = "РИПОЛ классик", Description = "" },
                    new Publisher { Name = "Феникс", Description = "" },
                    new Publisher { Name = "Ювента", Description = "" },
                    new Publisher { Name = "Фламинго", Description = "" },
                };

                db.Publishers.AddRange(publishers);
                db.SaveChanges();
            }
        }

        [HttpGet]
        public async Task<IActionResult> ReservationPanel()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UsersListPanel(int page = 1, string searchString = "",
            int itemsQuantity = 16, UsersSortState sortOrder = UsersSortState.IdAsc)
        {
            IEnumerable<User> users = await db.Users.Select(x => x)
                .Include(x => x.Reviews).Include(x => x.Role).ToListAsync();

            /* Фильтрация */
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();

                users = users.Where(u => u.Name.ToLower().Contains(searchString) 
                    || u.Email.ToLower().Contains(searchString));
            }

            /* Сортировка */
            var enumUsers = sortOrder switch
            {
                UsersSortState.IdAsc => users.OrderBy(u => u.Id),
                UsersSortState.IdDesc => users.OrderByDescending(u => u.Id),
                UsersSortState.RoleAsc => users.OrderBy(u => u.Role.Name),
                UsersSortState.RoleDesc => users.OrderByDescending(u => u.Role.Name),
                UsersSortState.NickAsc => users.OrderBy(u => u.Name),
                UsersSortState.NickDesc => users.OrderByDescending(u => u.Name),
                UsersSortState.EmailAsc => users.OrderBy(u => u.Email),
                UsersSortState.EmailDesc => users.OrderByDescending(u => u.Email),
                UsersSortState.ReviewsAsc => users.OrderBy(u => u.Reviews.Count),
                UsersSortState.ReviewsDesc => users.OrderByDescending(u => u.Reviews.Count),
                _ => users.OrderBy(u => u.Id)
            };

            /* Пагинация */
            int pageSize = itemsQuantity < 16 ? 16 : itemsQuantity;
            var count = users.Count();

            page = page < 1 ? 1 : page;

            users = users.Skip((page - 1) * pageSize).Take(pageSize);

            var viewModel = new UsersListViewModel
            {
                PageViewModel = new PageViewModel(count, page, pageSize),
                SortViewModel = new UsersSortViewModel(sortOrder),
                FilterViewModel = new FilterViewModel(searchString),
                Roles = await db.Roles.Select(x => x).ToListAsync(),
                ItemsQuantity = pageSize,
                Users = users.ToList()
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUser(int userId, UsersListViewModel model)
        {
            var user = await db.Users.FirstOrDefaultAsync(x => x.Id == userId);
            var role = await db.Roles.FirstOrDefaultAsync(x => x.Name == model.EditUserViewModel.RoleName);

            if (user != null && role != null)
            {
                user.Name = model.EditUserViewModel.Nick;
                user.Email = model.EditUserViewModel.Email;
                user.Password = model.EditUserViewModel.Password;
                user.Role = role;

                db.Users.Update(user);
                await db.SaveChangesAsync();
            }
            else
            {
                ModelState.AddModelError("", "Пользователь не найден.");
            }

            var referer = HttpContext.Request.Headers["Referer"].ToString();

            return Redirect(referer);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await db.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (user != null)
            {
                var reviews = await db.Reviews.Where(x => x.UserId == id).Select(x => x).ToListAsync();

                db.Reviews.RemoveRange(reviews);
                db.Users.Remove(user);
                await db.SaveChangesAsync();
            }

            var referer = HttpContext.Request.Headers["Referer"].ToString();

            return Redirect(referer);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteBook(int id)
        {
            var book = await db.Books.FirstOrDefaultAsync(b => b.Id == id);

            if (book != null)
            {
                db.Books.Remove(book);
                await db.SaveChangesAsync();
            }

            return RedirectToAction("librarypanel");
        }

        [HttpGet]
        public async Task<IActionResult> LibraryPanel(int page = 1, string searchString = "", 
            int itemsQuantity = 16, SortState sortOrder = SortState.Rating)
        {
            IQueryable<Book> bookQuery = db.Books
                .Include(b => b.BookAuthors).ThenInclude(sc => sc.Author).Select(x => x)
                .Include(b => b.BookGenres).ThenInclude(sc => sc.Genre).Select(x => x)
                .Include(b => b.BookPublishers).ThenInclude(sc => sc.Publisher).Select(x => x);

            IEnumerable<Book> books = await bookQuery.ToListAsync();

            /* Фильтрация по названию, авторам, издетелю, жанру */
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();

                books = books.Where(b => b.Name.ToLower().Contains(searchString)
                    || b.BookAuthors.AsEnumerable().Any(b => b.Author.FullName.ToLower().Contains(searchString))
                    || b.BookGenres.AsEnumerable().Any(b => b.Genre.Name.ToLower().Contains(searchString))
                    || b.BookPublishers.AsEnumerable().Any(b => b.Publisher.Name.ToLower().Contains(searchString)));
            }

            /* Сортировка книг по параметрам */
            books = sortOrder switch
            {
                SortState.NameAsc => books.OrderBy(b => b.Name),
                SortState.NameDesc => books.OrderByDescending(b => b.Name),
                SortState.IdAsc => books.OrderBy(b => b.Id),
                SortState.IdDesc => books.OrderByDescending(b => b.Id),
                SortState.Rating => books.OrderByDescending(b => b.Rating),
                SortState.Discussed => books.OrderByDescending(b => b),
                _ => books.OrderBy(b => b.Rating)
            };


            /* Пагинация таблицы */
            int pageSize = itemsQuantity < 8 ? 8 : itemsQuantity;
            var count = books.Count();

            page = page < 1 ? 1 : page;

            books = books.Skip((page - 1) * pageSize).Take(pageSize);

            var viewModel = new ListViewModel
            {
                PageViewModel = new PageViewModel(count, page, pageSize),
                SortViewModel = new BooksSortViewModel(sortOrder),
                GroupViewModel = null,
                FilterViewModel = null,
                ItemsQuantity = pageSize,
                Books = books.ToList()
            };

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> CreateBook(int? editableBookId = null, bool saveform = false)
        {
            CreateBookViewModel viewModel = null;

            if (editableBookId is null)
            {
                viewModel = new CreateBookViewModel
                {
                    AgeRequirement = null,
                    ReleaseDate = null,
                    Pages = null,
                    OnEdit = false,
                };
            }
            else
            {
                IQueryable<Book> bookQuery = db.Books
                    .Include(b => b.BookAuthors).ThenInclude(sc => sc.Author).Select(x => x)
                    .Include(b => b.BookGenres).ThenInclude(sc => sc.Genre).Select(x => x)
                    .Include(b => b.BookPublishers).ThenInclude(sc => sc.Publisher).Select(x => x);

                IEnumerable<Book> books = await bookQuery.ToListAsync();

                var book = books.FirstOrDefault(x => x.Id == editableBookId);

                IFormFile file = null;

                if (book.Image != null && book.Image.Length > 0)
                {
                    using (var stream = new MemoryStream(book.Image))
                    {
                        file = new FormFile(stream, 0, book.Image.Length, "old_image.png", "old_image.png");
                    }
                }

                var authors = new List<string>();
                var genres = new List<string>();
                var publishers = new List<string>();

                foreach (var author in book.BookAuthors)
                    authors.Add(author.AuthorId.ToString());

                foreach (var genre in book.BookGenres)
                    genres.Add(genre.GenreId.ToString());

                foreach (var publisher in book.BookPublishers)
                    publishers.Add(publisher.PublisherId.ToString());

                viewModel = new CreateBookViewModel
                {
                    Authors = System.Text.Json.JsonSerializer.Serialize(authors),
                    Genres = System.Text.Json.JsonSerializer.Serialize(genres),
                    Publishers = System.Text.Json.JsonSerializer.Serialize(publishers),
                    Image = file,
                    Name = book.Name,
                    Description = book.Description,
                    DetailedInfo = book.DetailedInfo,
                    AgeRequirement = book.AgeRequirement,
                    ReleaseDate = book.ReleaseDate,
                    Pages = book.Pages,
                    OnEdit = true,
                };
            }

            viewModel.BookListsViewModel = new BookListsViewModel
            {
                Authors = await db.Authors.Select(x => x).ToListAsync(),
                Genres = await db.Genres.Select(x => x).ToListAsync(),
                Publishers = await db.Publishers.Select(x => x).ToListAsync(),
            };

            viewModel.EditableBookId = editableBookId;
            ViewBag.SaveForm = saveform;

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBook(CreateBookViewModel model, int? editableBookId = null)
        {
            if (ModelState.IsValid)
            {
                // Перевод изображения в массив байтов

                var files = HttpContext.Request.Form.Files;
                byte[] bookImage = null;

                foreach (var image in files)
                {
                    if (image != null && image.Length > 0)
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            await image.OpenReadStream().CopyToAsync(memoryStream);

                            bookImage = memoryStream.ToArray();
                        }

                        break;
                    }
                }

                // Получение Json таблиц Авторов, Жанров, Издателей

                ICollection<Author> authors = null;
                ICollection<Genre> genres = null;
                ICollection<Publisher> publishers = null;

                try
                {
                    var authorsIdArray = System.Text.Json.JsonSerializer
                        .Deserialize<IEnumerable<string>>(model.Authors)
                        .Select(s => Int32.TryParse(s, out int n) ? n : (int?)null)
                        .Where(n => n.HasValue)
                        .Select(n => n.Value).ToList();

                    authors = await db.Authors.Where(a => authorsIdArray.Contains(a.Id)).ToListAsync();

                    var genresIdArray = System.Text.Json.JsonSerializer
                        .Deserialize<IEnumerable<string>>(model.Genres)
                        .Select(s => Int32.TryParse(s, out int n) ? n : (int?)null)
                        .Where(n => n.HasValue)
                        .Select(n => n.Value).ToList();

                    genres = await db.Genres.Where(a => genresIdArray.Contains(a.Id)).ToListAsync();

                    var publishersIdArray = System.Text.Json.JsonSerializer
                        .Deserialize<IEnumerable<string>>(model.Publishers)
                        .Select(s => Int32.TryParse(s, out int n) ? n : (int?)null)
                        .Where(n => n.HasValue)
                        .Select(n => n.Value).ToList();

                    publishers = await db.Publishers.Where(a => publishersIdArray.Contains(a.Id)).ToListAsync();
                }
                catch (Exception)
                {

                }

                // Создание экземпляра книги и добавление его в БД
                // Или если в метод передано значение editableBookId, обновить книгу в бд

                Book book = new Book();

                if (editableBookId != null)
                {
                    IQueryable<Book> bookQuery = db.Books
                        .Include(b => b.BookAuthors).ThenInclude(sc => sc.Author).Select(x => x)
                        .Include(b => b.BookGenres).ThenInclude(sc => sc.Genre).Select(x => x)
                        .Include(b => b.BookPublishers).ThenInclude(sc => sc.Publisher).Select(x => x);

                    IEnumerable<Book> books = await bookQuery.ToListAsync();

                    book = books.FirstOrDefault(x => x.Id == editableBookId);
                }

                book.Name = model.Name;
                book.Image = bookImage;
                book.Pages = model.Pages;
                book.Description = model.Description;
                book.ReleaseDate = model.ReleaseDate;
                book.DetailedInfo = model.DetailedInfo;
                book.AgeRequirement = model.AgeRequirement;

                if (editableBookId != null)
                {
                    book.BookAuthors.Clear();
                    book.BookGenres.Clear();
                    book.BookPublishers.Clear();
                }

                if (authors != null)
                    foreach (var a in authors)
                        book.BookAuthors.Add(new BookAuthor { BookId = book.Id, AuthorId = a.Id });

                if (genres != null)
                    foreach (var g in genres)
                        book.BookGenres.Add(new BookGenre { BookId = book.Id, GenreId = g.Id });

                if (publishers != null)
                    foreach (var p in publishers)
                        book.BookPublishers.Add(new BookPublisher { BookId = book.Id, PublisherId = p.Id });

                if (editableBookId != null)
                    db.Books.Update(book);
                else
                    await db.Books.AddAsync(book);

                await db.SaveChangesAsync();

                return Redirect($"/admin/librarypanel/");
            }

            return RedirectToAction("createbook");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddAuthor(Author model, int? editableBookId = null)
        {
            string fullName = model.FullName.ToLower();
            var authors = await db.Authors.ToListAsync();

            if (!authors.Any(a => a.FullName.ToLower().Contains(fullName)))
            {
                await db.Authors.AddAsync(model);
                await db.SaveChangesAsync();
            }
            else
                ModelState.AddModelError("", "Автор с таки именем уже существует!");

            return RedirectToAction("createbook", new { editableBookId = editableBookId, saveform = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddGenre(Genre model, int? editableBookId = null)
        {
            string name = model.Name.ToLower();
            var authors = await db.Genres.ToListAsync();

            if (!authors.Any(a => a.Name.ToLower().Contains(name)))
            {
                await db.Genres.AddAsync(model);
                await db.SaveChangesAsync();
            }
            else
                ModelState.AddModelError("", "Издатель с таки именем уже существует!");

            return RedirectToAction("createbook", new { editableBookId = editableBookId, saveform = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPublisher(Publisher model, int? editableBookId = null)
        {
            string name = model.Name.ToLower();
            var authors = await db.Publishers.ToListAsync();

            if (!authors.Any(a => a.Name.ToLower().Contains(name)))
            {
                await db.Publishers.AddAsync(model);
                await db.SaveChangesAsync();
            }
            else
                ModelState.AddModelError("", "Автор с таки именем уже существует!");

            return RedirectToAction("createbook", new { editableBookId = editableBookId, saveform = true } );
        }
    }
}
