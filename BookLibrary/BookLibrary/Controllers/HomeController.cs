﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using BookLibrary.Models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using BookLibrary.Models.View;

namespace BookLibrary.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationContext db;

        public HomeController(ApplicationContext context)
        {
            db = context;
        }

        public async Task<IActionResult> Index()
        {
            var books = await db.Books
                .Include(x => x.BookAuthors)
                .Include(x => x.Reviews)
                .Select(x => x)
                .Where(x => x.Rating >= 4)
                .Where(x => x.Reviews.Count >= 3)
                .ToListAsync();

            var model = new IndexViewModel
            {
                Books = books,
            };

            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Location()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
