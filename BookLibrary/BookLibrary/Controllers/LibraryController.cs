﻿using BookLibrary.Models;
using BookLibrary.Models.View;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookLibrary.Controllers
{
    public class LibraryController : Controller
    {
        private ApplicationContext db;

        public LibraryController(ApplicationContext context)
        {
            db = context;
        }

        [Route("library")]
        public async Task<IActionResult> List(
            int page = 1, string searchString = "", int itemsQuantity = 15,
            SortState sortOrder = SortState.Rating, 
            GroupState groupOrder = GroupState.None)
        {
            IQueryable<Book> bookQuery = db.Books.Include(b => b.BookAuthors)
                .ThenInclude(sc => sc.Author).Select(x => x)
                .Include(b => b.BookGenres).ThenInclude(sc => sc.Genre).Select(x => x)
                .Include(b => b.BookPublishers).ThenInclude(sc => sc.Publisher).Select(x => x)
                .Include(b => b.Reviews).ThenInclude(sc => sc.User).Select(x => x);

            IEnumerable<Book> books = await bookQuery.ToListAsync();

            // Фильтрация
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();

                books = books.Where(b => b.Name.ToLower().Contains(searchString) 
                    || b.BookAuthors.AsEnumerable().Any(b => b.Author.FullName.ToLower().Contains(searchString))
                    || b.BookPublishers.AsEnumerable().Any(b => b.Publisher.Name.ToLower().Contains(searchString)));
            }

            // Сортировка
            books = sortOrder switch
            {
                SortState.NameAsc   => books.OrderBy(b => b.Name),
                SortState.NameDesc  => books.OrderByDescending(b => b.Name),
                SortState.IdAsc     => books.OrderBy(b => b.ReleaseDate),
                SortState.IdDesc    => books.OrderByDescending(b => b.ReleaseDate),
                SortState.Rating    => books.OrderByDescending(b => b.Rating),
                SortState.Discussed => books.OrderByDescending(b => b.Reviews.Count),
                _ => books.OrderBy(b => b.Rating)
            };

            /*
            // Группировка
            if (groupOrder != GroupState.None)
            {
                var groupBooks = groupOrder switch
                {
                    GroupState.Author => books.GroupBy(b => b.Author),
                    GroupState.Category => books.GroupBy(b => b.Category),
                    GroupState.Availability => books.GroupBy(b => b.Category), // TODO: добавить группировку по доступности
                    _ => books.GroupBy(b => b.Author)
                };

                books = null;

                foreach (var item in groupBooks)
                {
                    foreach (var b in item)
                    {
                        books.Append(b);
                    }
                }
            }
           */
            // Пагинация
            int pageSize = itemsQuantity < 16 ? 16 : itemsQuantity;
            var count = books.Count();

            page = page < 1 ? 1 : page;

            books = books.Skip((page - 1) * pageSize).Take(pageSize);

            var viewModel = new ListViewModel
            {
                PageViewModel = new PageViewModel(count, page, pageSize),
                SortViewModel = new BooksSortViewModel(sortOrder),
                GroupViewModel = new GroupViewModel(groupOrder),
                FilterViewModel = new FilterViewModel(searchString),
                ItemsQuantity = pageSize,
                Books = books.ToList()
            };

            return View(viewModel);
        }

        public async Task<IActionResult> BookPage(int bookId, int reviewsQuantity = 4)
        {
            IQueryable<Book> bookQuery = db.Books.Include(b => b.BookAuthors)
                .ThenInclude(sc => sc.Author).Select(x => x)
                .Include(b => b.BookGenres).ThenInclude(sc => sc.Genre).Select(x => x)
                .Include(b => b.BookPublishers).ThenInclude(sc => sc.Publisher).Select(x => x)
                .Include(b => b.Reviews).ThenInclude(sc => sc.User).Select(x => x);

            var book = await bookQuery.FirstOrDefaultAsync(x => x.Id == bookId);

            if (book is null) return NotFound();

            ViewBag.ReviewsQuantity = reviewsQuantity;

            if (User.Identity.IsAuthenticated)
            {
                var index = book.Reviews.FindIndex(x => x.User.Name == User.Identity.Name);
                if (index != -1)
                {
                    var item = book.Reviews[index];
                    book.Reviews[index] = book.Reviews[0];
                    book.Reviews[0] = item;
                }
            }
            
            var viewModel = new BookPageViewModel() { Book = book, Review = new ReviewBookViewModel() };

            return View(viewModel);
        }
    }
}
