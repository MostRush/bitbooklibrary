﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BookLibrary.Models
{
    public class Author
    {
        [Key]
        public int Id { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage ="Введите имя автора!")]
        public string FirstName { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Введите фамилию автора!")]
        public string LastName { get; set; }
        public string Biography { get; set; }

        public string FullName { get => $"{FirstName} {LastName}"; }

        [JsonIgnore]
        public List<BookAuthor> BookAuthors { get; set; }

        public Author()
        {
            BookAuthors = new List<BookAuthor>();
        }
    }
}
