﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DetailedInfo { get; set; }
        public byte[] Image { get; set; }
        public double Rating { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int? Pages { get; set; }
        public int? AgeRequirement { get; set; }
        public List<Review> Reviews { get; set; }

        public List<BookAuthor> BookAuthors { get; set; }
        public List<BookGenre> BookGenres { get; set; }
        public List<BookPublisher> BookPublishers { get; set; }
        public List<BookReview> BookReviews { get; set; }

        public Book()
        {
            BookAuthors = new List<BookAuthor>();
            BookGenres = new List<BookGenre>();
            BookPublishers = new List<BookPublisher>();
            BookReviews = new List<BookReview>();
        }
    }
}
