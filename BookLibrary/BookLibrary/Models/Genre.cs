﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BookLibrary.Models
{
    public class Genre
    {
        public int Id { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage ="Введите название жанра!")]
        public string Name { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        public List<BookGenre> BookGenres { get; set; }

        public Genre()
        {
            BookGenres = new List<BookGenre>();
        }
    }
}