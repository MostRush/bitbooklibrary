﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BookLibrary.Models
{
    public class Publisher
    {
        public int Id { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Введите название издателя!")]
        public string Name { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        public List<BookPublisher> BookPublishers { get; set; }

        public Publisher()
        {
            BookPublishers = new List<BookPublisher>();
        }
    }
}
