﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace BookLibrary.Models
{
    public class Review
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }

        public double Rate { get; set; }
        public string Comment { get; set; }

        public DateTime PublicationDate { get; set; }

        [JsonIgnore]
        public List<BookReview> BookReviews { get; set; }

        public Review()
        {
            BookReviews = new List<BookReview>();
        }
    }
}
