﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public byte[] Avatar { get; set; }

        public List<Review> Reviews { get; set; }

        public int? RoleId { get; set; }
        public Role Role { get; set; }

        public User()
        {
            Reviews = new List<Review>();
        }
    }
}
