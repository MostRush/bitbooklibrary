﻿using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models.View.Authentication
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Не указана почта или никнейм")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Не указан пароль")]
        public string Password { get; set; }
    }
}
