﻿using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models.View.Authentication
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Не указан Никнейм")]
        [StringLength(16, MinimumLength = 3, 
            ErrorMessage = "Длина никнейма должна составлять от 3 до 16 символов")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Не указан Email")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Не указан пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }
    }
}
