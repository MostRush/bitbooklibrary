﻿using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models.View.Authentication
{
    public class RestoreViewModel
    {
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
    }
}
