﻿using System.Collections.Generic;

namespace BookLibrary.Models.View
{
    public class BookListsViewModel
    {
        public IEnumerable<Genre> Genres { get; set; }
        public IEnumerable<Author> Authors { get; set; }
        public IEnumerable<Publisher> Publishers { get; set; }
    }
}
