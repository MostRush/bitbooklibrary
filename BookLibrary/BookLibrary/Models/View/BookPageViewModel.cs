﻿namespace BookLibrary.Models.View
{
    public class BookPageViewModel
    {
        public Book Book { get; set; }
        public ReviewBookViewModel Review { get; set; }
    }
}
