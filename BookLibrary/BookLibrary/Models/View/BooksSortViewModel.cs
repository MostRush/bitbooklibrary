﻿using BookLibrary.Models.View.Interfaces;
using System;

namespace BookLibrary.Models
{
    public enum SortState
    {
        NameAsc,
        NameDesc,
        Rating,
        IdAsc,
        IdDesc,
        Discussed
    }

    public class BooksSortViewModel : ISortViewModel
    {
        public int CurrentState { get; set; } = (int)SortState.Rating;

        public Type TypeOfSort { get; set; } = typeof(SortState);

        public BooksSortViewModel(SortState sortOrder)
        {
            CurrentState = (int)sortOrder;
        }

        public string GetSortStateName(int sortState)
        {
            string sortName = (SortState)sortState switch
            {
                SortState.NameAsc   => "По названию (А - Я)",
                SortState.NameDesc  => "По названию (Я - А)",
                SortState.IdAsc     => "По новизне (сначала старые)",
                SortState.IdDesc    => "По новизне (сначала новые)",
                SortState.Rating    => "По рейтингу",
                SortState.Discussed => "Обсуждаемые",
                _ => "сортировать"
            };

            return sortName;
        }
    }
}
