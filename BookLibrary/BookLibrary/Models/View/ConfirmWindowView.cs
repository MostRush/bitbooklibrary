﻿namespace BookLibrary.Models.View
{
    public class ConfirmWindowView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Messeage { get; set; }
        public string Route { get; set; }
    }
}
