﻿using System;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models.View
{
    public class CreateBookViewModel
    {
        [StringLength(100)]
        [Display(Name = "Название")]
        [Required(ErrorMessage = "Введите название книги!")]
        public string Name { get; set; }

        [StringLength(3000)]
        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Введите описание книги!")]
        public string Description { get; set; }

        [StringLength(3000)]
        [Display(Name = "Подробная информация")]
        [Required(ErrorMessage = "Введите подрообную информацию о книге!")]
        public string DetailedInfo { get; set; }

        [Display(Name = "Количество сраниц")]
        [Range(0, int.MaxValue, ErrorMessage = "Неверное значение!")]
        [Required(ErrorMessage = "Укажите количество страниц!")]
        public int? Pages { get; set; }

        [Display(Name = "Возрастное ограничение")]
        [Range(0, 120, ErrorMessage = "Неверное значение!")]
        [Required(ErrorMessage = "Укажите возрастное ограничение!")]
        public int? AgeRequirement { get; set; }

        [Display(Name = "Дата выхода")]
        [Required(ErrorMessage = "Укажите дату выпуска!")]
        public DateTime? ReleaseDate { get; set; }

        [Display(Name = "Обложка книги")]
        [Required(ErrorMessage = "Выберите изображение обложки!")]
        public IFormFile Image { get; set; }

        public bool? OnEdit { get; set; }
        public int? EditableBookId { get; set; }

        public string Genres { get; set; }
        public string Authors { get; set; }
        public string Publishers { get; set; }

        public Author Author { get; set; }
        public Genre Genre { get; set; }
        public Publisher Publisher { get; set; }

        public BookListsViewModel BookListsViewModel { get; set; }
    }
}
