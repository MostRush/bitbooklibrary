﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookLibrary.Models.View
{
    public class EditUserViewModel
    {
        [Required(ErrorMessage = "Не указано имя пользователя")]
        [StringLength(16, MinimumLength = 3,
               ErrorMessage = "Длина никнейма должна составлять от 3 до 16 символов")]
        public string Nick { get; set; }

        [Required(ErrorMessage = "Не указан Email")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Не указан пароль")]
        public string Password { get; set; }

        [Required]
        public string RoleName { get; set; }
    }
}
