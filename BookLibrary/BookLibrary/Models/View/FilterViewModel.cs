﻿using Microsoft.AspNetCore.Mvc;

namespace BookLibrary.Models
{
    public class FilterViewModel
    {
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }

        public FilterViewModel(string searchString)
        {
            SearchString = searchString;
        }
    }
}
