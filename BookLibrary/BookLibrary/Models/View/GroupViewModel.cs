﻿namespace BookLibrary.Models
{
    public enum GroupState
    { 
        None,
        Author,
        Category,
        Availability,
    }

    public class GroupViewModel
    {
        public GroupState CurrentState { get; set; } = GroupState.None;

        public GroupViewModel(GroupState groupOrder)
        {
            CurrentState = groupOrder;
        }

        public string GetSortStateName(GroupState sortState)
        {
            string sortName = sortState switch
            {
                GroupState.None         => "Не группировать",
                GroupState.Author       => "По автору",
                GroupState.Category     => "По жанру",
                GroupState.Availability => "По доступности",
                _ => "группировать"
            };

            return sortName;
        }
    }
}
