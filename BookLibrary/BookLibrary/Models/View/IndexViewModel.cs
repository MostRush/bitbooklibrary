﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookLibrary.Models.View
{
    public class IndexViewModel
    {
        public List<Book> Books { get; set; }
    }
}
