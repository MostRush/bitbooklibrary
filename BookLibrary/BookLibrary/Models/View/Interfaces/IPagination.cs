﻿namespace BookLibrary.Models.View.Interfaces
{
    public interface IPagination
    {
        public PageViewModel PageViewModel { get; set; }
        public ISortViewModel SortViewModel { get; set; }
        public GroupViewModel GroupViewModel { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public int ItemsQuantity { get; set; }
    }
}
