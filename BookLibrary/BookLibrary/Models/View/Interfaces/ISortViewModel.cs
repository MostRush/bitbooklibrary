﻿using System;

namespace BookLibrary.Models.View.Interfaces
{
    public interface ISortViewModel
    {
        public Type TypeOfSort { get; set; }
        public int CurrentState { get; set; }
        public string GetSortStateName(int sortState);
    }
}
