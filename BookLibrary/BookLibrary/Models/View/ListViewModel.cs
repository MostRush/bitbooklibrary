﻿using BookLibrary.Models.View.Interfaces;
using System.Collections.Generic;

namespace BookLibrary.Models
{
    public class ListViewModel : IPagination
    {
        public ICollection<Book> Books { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public GroupViewModel GroupViewModel { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public ISortViewModel SortViewModel { get; set; }
        public int ItemsQuantity { get; set; }
    }
}
