﻿using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models.View
{
    public class ReviewBookViewModel
    {
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Коментарий не может быть пустым!")]
        [StringLength(3000, ErrorMessage = "Длина коментария превышиет 3000 символов!")]
        public string Comment { get; set; }

        [Range(0.5, 5, ErrorMessage = "Ошибка: неверное значение!")]
        [Required(ErrorMessage = "Поставте оценку!")]
        public double? RateValue { get; set; }
    }
}
