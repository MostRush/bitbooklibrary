﻿using BookLibrary.Models.View.Interfaces;
using System;

namespace BookLibrary.Models.View
{
    public enum ReviewSortState
    {
        DateAsc,
        DateDesc,
        RatingAsc,
        RatingDesc,
    }

    public class ReviewSortViewModel : ISortViewModel
    {
        public int CurrentState { get; set; } = (int)ReviewSortState.DateDesc;
        public Type TypeOfSort { get; set; } = typeof(ReviewSortState);

        public ReviewSortViewModel(ReviewSortState sortOrder)
        {
            CurrentState = (int)sortOrder;
        }

        public string GetSortStateName(int sortState)
        {
            string sortName = (ReviewSortState)sortState switch
            {
                ReviewSortState.DateAsc => "По возрвстанию даты",
                ReviewSortState.DateDesc => "По убыванию даты",
                ReviewSortState.RatingAsc => "По возрастанию рейтинга",
                ReviewSortState.RatingDesc => "По убыванию рейтинга",
                _ => "сортировать"
            };

            return sortName;
        }
    }
}
