﻿using BookLibrary.Models.View.Interfaces;
using System.Collections.Generic;

namespace BookLibrary.Models.View
{
    public class UserRviewsViewModel : IPagination
    {
        public User User { get; set; }
        public List<Review> Reviews { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public ISortViewModel SortViewModel { get; set; }
        public GroupViewModel GroupViewModel { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public int ItemsQuantity { get; set; }
    }
}
