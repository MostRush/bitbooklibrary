﻿using BookLibrary.Models.View.Authentication;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models.View
{
    public class UserSettingsViewModel
    {
        public User User { get; set; }

        [Required(ErrorMessage = "Не указан Никнейм")]
        [StringLength(16, MinimumLength = 3,
            ErrorMessage = "Длина имени должна составлять от 3 до 16 символов")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Не указана электронная почта")]
        public string Email { get; set; }

        public IFormFile Avatar { get; set; }

        [StringLength(3000, ErrorMessage = "Описание слишком длинное. " +
            "Оно не может превышать больше 3000 символов")]
        public string Description { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Укажите старый пароль")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Укажите новый пароль")]
        public string NewPassword { get; set; }
    }
}
