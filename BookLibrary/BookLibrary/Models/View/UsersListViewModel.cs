﻿using BookLibrary.Models.View;
using BookLibrary.Models.View.Authentication;
using BookLibrary.Models.View.Interfaces;
using System.Collections.Generic;

namespace BookLibrary.Models
{
    public class UsersListViewModel : IPagination
    {
        public ICollection<User> Users { get; set; }
        public List<Role> Roles { get; set; }
        public int ItemsQuantity { get; set; }
        public EditUserViewModel EditUserViewModel { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public ISortViewModel SortViewModel { get; set; }
        public GroupViewModel GroupViewModel { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public RegisterViewModel RegisterViewModel { get; set; }
    }
}
