﻿using BookLibrary.Models.View.Interfaces;
using System;

namespace BookLibrary.Models
{
    public enum UsersSortState
    {
        IdAsc,
        IdDesc,
        RoleAsc,
        RoleDesc,
        NickAsc,
        NickDesc,
        EmailAsc,
        EmailDesc,
        ReviewsAsc,
        ReviewsDesc,
    }

    public class UsersSortViewModel : ISortViewModel
    {
        public int CurrentState { get; set; } = (int)UsersSortState.IdAsc;
        public Type TypeOfSort { get; set; } = typeof(UsersSortState);

        public UsersSortViewModel(UsersSortState sortOrder)
        {
            CurrentState = (int)sortOrder;
        }

        public string GetSortStateName(int sortState)
        {
            string sortName = (UsersSortState)sortState switch
            {
                UsersSortState.IdAsc => "По возрастанию ID",
                UsersSortState.IdDesc => "По убыванию ID",
                UsersSortState.RoleAsc => "По роли (А - Я)",
                UsersSortState.RoleDesc => "По роли (Я - А)",
                UsersSortState.NickAsc => "По ник-нейму (А - Я)",
                UsersSortState.NickDesc => "По ник-нейму (Я - А)",
                UsersSortState.EmailAsc => "По почте (А - Я)",
                UsersSortState.EmailDesc => "По почте (Я - А)",
                UsersSortState.ReviewsAsc => "По возрастанию количества отзывов",
                UsersSortState.ReviewsDesc => "По убыванию количества отзывов",
                _ => "сортировать"
            };

            return sortName;
        }
    }
}
