﻿using System;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace BookLibrary.TagHelpers
{
    [HtmlTargetElement("avatar-img")]
    public class AvatarTagHelper : TagHelper
    {
        public byte[] Image { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            string avatar = Image != null ? "data:image/jpeg;base64,"
                + Convert.ToBase64String(Image) : "/img/u_user.png";

            output.TagName = "img";
            output.Attributes.Add(new TagHelperAttribute("src", avatar));
        }
    }
}
