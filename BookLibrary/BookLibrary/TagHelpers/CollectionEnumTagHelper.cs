﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookLibrary.Models;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace BookLibrary.TagHelpers
{
    [HtmlTargetElement("collection-enum")]
    public class CollectionEnumTagHelper : TagHelper
    {
        public Book Book { get; set; }
        public Type ListType { get; set; }
        public bool SetTypeName { get; set; }
        public int MaxOutput { get; set; } = 10;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            string _string = "";

            if (ListType.Equals(typeof(Author)))
            {
                var authors = Book.BookAuthors.Select(a => a.Author).ToList();
                _string = GetAuthorsString(authors);
            }
            else if (ListType.Equals(typeof(Genre)))
            {
                var genres = Book.BookGenres.Select(a => a.Genre).ToList();
                _string = GetGenresString(genres);
            }
            else if (ListType.Equals(typeof(Publisher)))
            {
                var publishers = Book.BookPublishers.Select(a => a.Publisher).ToList();
                _string = GetPublishersString(publishers);
            }
            else
                return;

            output.Content.Append(_string);
        }

        private string GetAuthorsString(List<Author> authors)
        {
            var count = authors.Count();

            string _string = "";

            if (authors != null && count > 0)
            {
                if (SetTypeName)
                    _string = count > 1 ? "Авторы: " : "Автор: ";

                for (int i = 0; i < count; i++)
                {
                    if (i + 1 > MaxOutput) { _string += " и др."; break; }
                    _string += $"{authors[i]?.FullName}{(count.Equals(i + 1) ? "" : ", ")}";
                }
            }
            else
                _string = "Автор не указан";

            return _string;
        }

        private string GetGenresString(List<Genre> genres)
        {
            var count = genres.Count();

            string _string = "";

            if (genres != null && count > 0)
            {
                if (SetTypeName)
                    _string = count > 1 ? "Жанры: " : "Жанр: ";

                for (int i = 0; i < count; i++)
                {
                    if (i + 1 > MaxOutput) { _string += " и др."; break; }
                    _string += $"{genres[i]?.Name}{(count.Equals(i + 1) ? "" : ", ")}";
                }
            }
            else
                _string = "Жанр не указан";

            return _string;
        }

        private string GetPublishersString(List<Publisher> publishers)
        {
            var count = publishers.Count();

            string _string = "";

            if (publishers != null && count > 0)
            {
                if (SetTypeName)
                    _string = count > 1 ? "Издатели: " : "Издатель: ";

                for (int i = 0; i < count; i++)
                {
                    if (i + 1 > MaxOutput) { _string += " и др."; break; }
                    _string += $"{publishers[i]?.Name}{(count.Equals(i + 1) ? "" : ", ")}";
                }
            }
            else
                _string = "Издатель не указан";

            return _string;
        }
    }
}
