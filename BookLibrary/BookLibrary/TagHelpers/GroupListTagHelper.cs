﻿using BookLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace BookLibrary.TagHelpers
{
    [HtmlTargetElement("group-list")]
    public class GroupListTagHelper : TagHelper
    {
        public GroupState Current { get; set; }
        public string PageAction { get; set; }

        private IUrlHelperFactory urlHelperFactory;

        public GroupListTagHelper(IUrlHelperFactory helperFactory)
        {
            urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewCotnext { get; set; }
        public ListViewModel ListViewModel { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var urlHelper = urlHelperFactory.GetUrlHelper(ViewCotnext);

            output.TagName = "ul";
            output.Attributes.Add("class", "list-group");

            Current = ListViewModel.GroupViewModel.CurrentState;

            GroupState[] groupStatesOrder =
            {
                GroupState.None,
                GroupState.Author,
                GroupState.Category,
                GroupState.Availability,
            };

            foreach (var g in groupStatesOrder)
            {
                var tag = new TagBuilder("li");

                tag.InnerHtml.Append(ListViewModel.GroupViewModel.GetSortStateName(g));
                tag.AddCssClass("list-group-item");

                if (g == Current)
                    tag.AddCssClass("sort-list-button-current");
                else
                    tag.AddCssClass("sort-list-button");

                string url = urlHelper.Action(PageAction, new { 
                    groupOrder = g,
                    sortOrder = ListViewModel.SortViewModel.CurrentState,
                    searchString = ListViewModel.FilterViewModel.SearchString,
                    itemsQuantity = ListViewModel.ItemsQuantity,
                });

                if (g != Current)
                    tag.Attributes.Add("onmousedown", $"window.location='{url}'");

                output.Content.AppendHtml(tag);
            }
        }
    }
}
