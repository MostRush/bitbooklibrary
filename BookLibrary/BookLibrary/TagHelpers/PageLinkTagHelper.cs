﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using BookLibrary.Models.View.Interfaces;
using BookLibrary.Models;

namespace BookLibrary.TagHelpers
{
    enum PageLinkArrow
    {
        Right, Left, None, ToFirst, ToLast
    }

    [HtmlTargetElement("page-link")]
    public class PageLinkTagHelper : TagHelper
    {
        private IUrlHelperFactory urlHelperFactory;

        public PageLinkTagHelper(IUrlHelperFactory helperFactory)
        {
            urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }
        public PageViewModel PageModel { get; set; }
        public IPagination ViewModel { get; set; }
        public string PageAction { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var urlHelper = urlHelperFactory.GetUrlHelper(ViewContext);

            output.TagName = "div";
            output.Attributes.Add(new TagHelperAttribute("class", "d-flex"));

            // набор ссылок будет представлять список ul
            var tag = new TagBuilder("div");
            tag.AddCssClass("pagination-my");

            // перывая страница
            var firstItem = CreateTag(1, urlHelper, PageLinkArrow.ToFirst);
            tag.InnerHtml.AppendHtml(firstItem);

            // пред. страница
            var prevItem = CreateTag(PageModel.PageNumber - 1, urlHelper, PageLinkArrow.Left);
            tag.InnerHtml.AppendHtml(prevItem);

            #region Доступные страницы

            int maxPages = 9;

            int curPage = PageModel.PageNumber;
            int fPage = curPage - maxPages / 2;
            fPage = fPage < 1 ? 1 : fPage;

            // страницы перед текущей
            for (int i = fPage; i <= curPage - 1; i++)
            {
                var currentItem = CreateTag(i, urlHelper, PageLinkArrow.None);
                tag.InnerHtml.AppendHtml(currentItem);
            }

            // страницы после текущей и текущая
            int restrCounter = 1;
            for (int i = curPage; i <= PageModel.TotalPages; i++)
            {
                if (restrCounter > maxPages - (curPage - fPage)) break;

                var currentItem = CreateTag(i, urlHelper, PageLinkArrow.None);
                tag.InnerHtml.AppendHtml(currentItem);

                restrCounter++;
            }

            if (PageModel.TotalPages <= 0)
            {
                var currentItem = CreateTag(1, urlHelper, PageLinkArrow.None);
                tag.InnerHtml.AppendHtml(currentItem);
            }

            #endregion

            // след. страница
            var nextItem = CreateTag(PageModel.PageNumber + 1, urlHelper, PageLinkArrow.Right);
            tag.InnerHtml.AppendHtml(nextItem);

            // последняя страница
            var lastItem = CreateTag(PageModel.TotalPages, urlHelper, PageLinkArrow.ToLast);
            tag.InnerHtml.AppendHtml(lastItem);

            output.Content.AppendHtml(tag);
        }

        private TagBuilder CreateTag(int pageNumber, IUrlHelper urlHelper, 
            PageLinkArrow pageLinkArrow = PageLinkArrow.None)
        {
            var item = new TagBuilder("a");

            if ((pageLinkArrow == PageLinkArrow.None) && (pageNumber == this.PageModel.PageNumber))
                item.AddCssClass("active");
            else
                item.Attributes["href"] = urlHelper.Action(PageAction, new { 
                    page = pageNumber,
                    sortOrder = ViewModel.SortViewModel?.CurrentState,
                    groupOrder = ViewModel.GroupViewModel?.CurrentState,
                    searchString = ViewModel.FilterViewModel?.SearchString,
                    itemsQuantity = ViewModel.ItemsQuantity,
                });

            string symbol = pageLinkArrow switch
            {
                PageLinkArrow.Left      => "‹",
                PageLinkArrow.Right     => "›",
                PageLinkArrow.ToFirst   => "«",
                PageLinkArrow.ToLast    => "»",
                _ => pageNumber.ToString()
            };
            
            if (pageNumber < 1 || pageNumber > PageModel.TotalPages)
            {
                item.Attributes.Remove("href");
                item.Attributes.Add("style", "cursor: not-allowed;");
            } 
            else if ((pageLinkArrow == PageLinkArrow.ToFirst && PageModel.PageNumber == 1) ||
                (pageLinkArrow == PageLinkArrow.ToLast && PageModel.PageNumber == PageModel.TotalPages))
            {
                item.Attributes.Remove("href");
                item.Attributes.Add("style", "cursor: not-allowed;");
            }

            item.InnerHtml.AppendHtml(symbol);

            return item;
        }
    }
}
