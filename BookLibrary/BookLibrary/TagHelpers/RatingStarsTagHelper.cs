﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;

namespace BookLibrary.TagHelpers
{
    static internal class StarState
    {
        public static string Full { get; } = "on";
        public static string None { get; } = "off";
        public static string Half { get; } = "half";
    }

    public class RatingStarsTagHelper : TagHelper
    {
        public double? Rating { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Attributes.SetAttribute("style", "float: left;");

            Rating = Rating > 5 ? 5 : Rating;
            Rating = Rating < 0 ? 0 : Rating;
            Rating = Convert.ToDouble(Rating?.ToString("N1"));

            int leftNumSide  = (int)Rating;
            int rightNumSide = (int)Math.Round((Rating ?? 0) % 1 * 100) / 10;

            for (int i = 0; i < 5; i++)
            {
                var state = StarState.None;

                state = i >= leftNumSide ? state : StarState.Full;

                if (i == leftNumSide)
                    state = rightNumSide >= 5 ? StarState.Half : state;

                output.Content.AppendHtml($"<span class='fas star {state}'></span>");
            }
        }
    }
}
