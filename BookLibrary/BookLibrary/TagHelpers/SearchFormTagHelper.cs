﻿using BookLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace BookLibrary.TagHelpers
{
    [HtmlTargetElement("search-form")]
    public class SearchFormTagHelper : TagHelper
    {
        private IUrlHelperFactory urlHelperFactory;
        public string PageController { get; set; }
        public string PageAction { get; set; }

        public SearchFormTagHelper(IUrlHelperFactory urlHelperFactory)
        {
            this.urlHelperFactory = urlHelperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public ListViewModel ListViewModel { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var urlHelper = urlHelperFactory.GetUrlHelper(ViewContext);

            output.TagName = "form";
            output.Attributes.Add("method", "get");

            var div = new TagBuilder("div");
            div.AddCssClass("input-group");
            div.Attributes.Add("style", "padding: 2%");

            var input = new TagBuilder("input");
            input.AddCssClass("form-group");
            input.AddCssClass("search-input");
            input.Attributes.Add("placeholder", "Найти");
            input.Attributes.Add("value", "");

            string url = urlHelper.Action(PageAction, new
            {
                searchString = input.Attributes["values"],
                sortOrder = ListViewModel.SortViewModel.CurrentState
            });

            div.InnerHtml.AppendHtml(input);
            output.Content.AppendHtml(div);
        }
    }
}
