﻿using BookLibrary.Models;
using BookLibrary.Models.View.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;

namespace BookLibrary.TagHelpers
{
    [HtmlTargetElement("sort-list")]
    public class SortListTagHelper : TagHelper
    {
        public int Current { get; set; }
        public string PageAction { get; set; }
        public Dictionary<string, string> RouteData { get; set; }

        private IUrlHelperFactory urlHelperFactory;

        public SortListTagHelper(IUrlHelperFactory helperFactory)
        {
            urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }
        public IPagination ViewModel { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var urlHelper = urlHelperFactory.GetUrlHelper(ViewContext);

            output.TagName = "ul";
            output.Attributes.Add("class", "list-group");

            Current = ViewModel.SortViewModel.CurrentState;

            Type typeOfSort = ViewModel.SortViewModel.TypeOfSort;

            foreach (var s in Enum.GetValues(typeOfSort))
            {
                var tag = new TagBuilder("li");

                tag.InnerHtml.Append(ViewModel.SortViewModel.GetSortStateName((int)s));
                tag.AddCssClass("list-group-item");

                if ((int)s == Current)
                    tag.AddCssClass("sort-list-button-current");
                else
                    tag.AddCssClass("sort-list-button");

                var route = new Dictionary<string, string>(RouteData ?? new Dictionary<string, string>())
                {
                    { "sortOrder", s.ToString() },
                    { "groupOrder", ViewModel?.GroupViewModel?.CurrentState.ToString() },
                    { "searchString", ViewModel?.FilterViewModel?.SearchString?.ToString() },
                    { "itemsQuantity", ViewModel?.ItemsQuantity.ToString() }
                };

                string url = urlHelper.Action(PageAction, route);

                if ((int)s != Current)
                    tag.Attributes.Add("onmousedown", $"window.location='{url}'");

                output.Content.AppendHtml(tag);
            }
        }
    }
}
